const { PubSub } = require('@google-cloud/pubsub');

const pubsub = new PubSub();

const subscriptionName = 'worker-1';
const timeout = 60;

//step 1: init subscription
const subscription = pubsub.subscription(subscriptionName);
let messageCount = 0;

const messageHandler = message => {
  console.log(`Received message ${message.id}:`);
  console.log(`\tData: ${message.data}`);
  console.log(`\tAttributes: ${message.attributes}`);
  messageCount += 1;

  // acknowlodge
  message.ack();
};

// step 2: use messageHandler into subscription
subscription.on('message', messageHandler);

setTimeout(() => {
  subscription.removeListener('message', messageHandler);
  console.log(`${messageCount} message(s) received.`);
}, timeout * 10000);